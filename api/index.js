import {Router} from 'express';
import transaction from './routes/transaction';
import transactionType from "./routes/transactionType";
import owner from "./routes/owner";
import bank from "./routes/bank";
import account from "./routes/account";

export default () => {
    const app = Router();
    transaction(app);
    transactionType(app);
    owner(app);
    bank(app);
    account(app);

    return app;
}