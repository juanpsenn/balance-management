import {Router} from "express";
import TransactionTypeServices from "../../services/transactiontype.services";
import {getResponseOrNotFound, internalErrorResponse} from "../../helpers/response.helpers";
import TransactionService from "../../services/transaction.services";

const route = Router();

export default (app) => {
    app.use('/transaction-types', route);

    route.post('', async (req, res) => {
        try {
            const {transactionType} = await TransactionTypeServices.createTransactionType(req.body);
            return getResponseOrNotFound({data: transactionType, res})
        } catch (e) {
            return internalErrorResponse({e, res})
        }
    });

    route.get('/', async (req, res) => {
        try {
            const {transactionTypes} = await TransactionTypeServices.listTransactionTypes();
            return getResponseOrNotFound({data: transactionTypes, res, many: true});
        } catch (e) {
            return internalErrorResponse({e, res})
        }
    })

    route.get('/:id', async (req, res) => {
        try {
            const transactionTypeId = req.params.id
            const {transactionType} = await TransactionTypeServices.getTransactionType(transactionTypeId);
            return getResponseOrNotFound({data: transactionType, res});
        } catch (e) {
            return internalErrorResponse({e, res})
        }
    })
}