import {Router} from "express";
import {getResponseOrNotFound, internalErrorResponse} from "../../helpers/response.helpers";
import BankServices from "../../services/bank.services";

const route = Router();

export default (app) => {
    app.use('/banks', route);

    route.post('', async (req, res) => {
        try {
            const {bank} = await BankServices.createBank(req.body);
            return getResponseOrNotFound({data: bank, res})
        } catch (e) {
            return internalErrorResponse({e, res})
        }
    });

    route.get('/', async (req, res) => {
        try {
            const {banks} = await BankServices.listBanks();
            return getResponseOrNotFound({data: banks, res, many: true});
        } catch (e) {
            return internalErrorResponse({e, res})
        }
    })

    route.get('/:id', async (req, res) => {
        try {
            const bankId = req.params.id
            const {bank} = await BankServices.getBank(bankId);
            return getResponseOrNotFound({data: bank, res});
        } catch (e) {
            return internalErrorResponse({e, res})
        }
    })
}