import {Router} from "express";
import TransactionService from "../../services/transaction.services";
import {getResponseOrNotFound, internalErrorResponse, postResponse} from "../../helpers/response.helpers";

const route = Router();

export default (app) => {
    app.use('/transactions', route);

    route.post('/', async (req, res) => {
        try {
            const {transaction} = await TransactionService.registerTransaction(req.body);
            return postResponse({data: transaction, res})
        } catch (e) {
            return internalErrorResponse({e, res})
        }
    })

    route.get('/', async (req, res) => {
        try {
            const {transactions} = await TransactionService.listTransactions();
            return getResponseOrNotFound({data: transactions, res, many: true});
        } catch (e) {
            return internalErrorResponse({e, res})
        }
    })

    route.get('/:id', async (req, res) => {
        try {
            const transactionId = req.params.id
            const {transaction} = await TransactionService.getTransaction(transactionId);
            return getResponseOrNotFound({data: transaction, res});
        } catch (e) {
            return internalErrorResponse({e, res})
        }
    })
}