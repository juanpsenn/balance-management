import {Router} from "express";
import {getResponseOrNotFound, internalErrorResponse} from "../../helpers/response.helpers";
import OwnerServices from "../../services/owner.services";

const route = Router();

export default (app) => {
    app.use('/owners', route);

    route.post('', async (req, res) => {
        try {
            const {owner} = await OwnerServices.createOwner(req.body);
            return getResponseOrNotFound({data: owner, res})
        } catch (e) {
            return internalErrorResponse({e, res})
        }
    });

    route.get('/', async (req, res) => {
        try {
            const {owners} = await OwnerServices.listOwners();
            return getResponseOrNotFound({data: owners, res, many: true});
        } catch (e) {
            return internalErrorResponse({e, res})
        }
    })

    route.get('/:id', async (req, res) => {
        try {
            const ownerId = req.params.id
            const {owner} = await OwnerServices.getOwner(ownerId);
            return getResponseOrNotFound({data: owner, res});
        } catch (e) {
            return internalErrorResponse({e, res})
        }
    })
}