import {Router} from "express";
import {getResponseOrNotFound, internalErrorResponse} from "../../helpers/response.helpers";
import AccountServices from "../../services/account.services";

const route = Router();

export default (app) => {
    app.use('/accounts', route);

    route.post('', async (req, res) => {
        try {
            const {account} = await AccountServices.createAccount(req.body);
            return getResponseOrNotFound({data: account, res})
        } catch (e) {
            return internalErrorResponse({e, res})
        }
    });

    route.get('/', async (req, res) => {
        try {
            const {accounts} = await AccountServices.listAccounts();
            return getResponseOrNotFound({data: accounts, res, many: true});
        } catch (e) {
            return internalErrorResponse({e, res})
        }
    })

    route.get('/:id', async (req, res) => {
        try {
            const accountId = req.params.id
            const {account} = await AccountServices.getAccount(accountId);
            return getResponseOrNotFound({data: account, res});
        } catch (e) {
            return internalErrorResponse({e, res})
        }
    })
}