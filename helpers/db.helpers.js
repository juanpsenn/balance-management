import BankServices from "../services/bank.services";
import OwnerServices from "../services/owner.services";
import TransactionTypeServices from "../services/transactiontype.services";

export const initDb = async () => {
    const owners = [
        {name: 'Juan Pablo Senn'},
        {name: 'Diego Abanto'},
        {name: 'Enzo Villarreal'},
        {name: 'Mickaela Crespo'},
        {name: 'Eliana Garzia'},
    ];

    const banks = [
        {name: 'Santander'},
        {name: 'Mercado Pago'},
        {name: 'Brubank'},
        {name: 'Galicia'},
    ];

    const transactiontypes = [
        {name: 'Pago de servidor'},
        {name: 'Pago de comision'},
    ]

    banks.forEach(async bank => await BankServices.createBank(bank))
    owners.forEach(async owner => await OwnerServices.createOwner(owner))
    transactiontypes.forEach(async type => await TransactionTypeServices.createTransactionType(type))
}