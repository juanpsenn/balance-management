export const getResponseOrNotFound = ({data, res, many = false}) => {
    const response = (condition, data, res) =>
        condition ?
            res.status(200).json({items: data}) :
            res.status(204).json({detail: `No results found`})
    if (many) {
        return response(
            data.length !== 0,
            data,
            res);
    } else {
        return response(
            data,
            data,
            res);
    }
};

export const postResponse = ({data, res}) => {
    return res.status(201).json({data})
}

export const internalErrorResponse = ({e, res}) => {
    return res.status(500).json({
        detail: e.message
    })
}