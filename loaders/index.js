import mongooseLoader from './mongoose';
import expressLoader from './express';

export default async (expressApp) => {
    const mongoConnection = await mongooseLoader();
    await expressLoader(expressApp);
}