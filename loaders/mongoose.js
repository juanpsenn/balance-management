import mongoose from "mongoose";
import db from "../config/db";

export default async () => {
    const connection = await mongoose
        .connect(db, {
            useNewUrlParser: true,
            useCreateIndex: true,
            useFindAndModify: false,
            useUnifiedTopology: true
        });
    return connection.connection.db;
}