import express from "express";
import cors from "cors";

import routes from '../api';
import {prefix} from '../config/api';

import 'dotenv/config';

export default (app) => {
    app.use(cors());

    app.use(express.json({limit: '50mb'}));
    app.use(express.urlencoded({extended: true, limit: '50mb'}))

    app.use(prefix, routes());
    app.use((req, res) => {
        res.status(404).send('404: Page not found')
    });
}
