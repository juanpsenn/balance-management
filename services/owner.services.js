import Owner from "../models/owner";

export default class OwnerServices {
    static async createOwner(owner) {
        const ownerRecord = await Owner.create(owner);
        return {owner: ownerRecord};
    }

    static async listOwners() {
        const ownerRecords = await Owner.find({})
        return {owners: ownerRecords};
    };

    static async getOwner(ownerId) {
        const ownerRecord = await Owner.findById(ownerId);
        return {owner: ownerRecord};
    };
}