import Bank from "../models/bank";

export default class BankServices {
    static async createBank(bank) {
        const bankRecord = await Bank.create(bank);
        return {bank: bankRecord};
    }

    static async listBanks() {
        const bankRecords = await Bank.find({})
        return {banks: bankRecords};
    };

    static async getBank(bankId) {
        const bankRecord = await Bank.findById(bankId);
        return {bank: bankRecord};
    };
}