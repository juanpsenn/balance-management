import TransactionType from "../models/transactionType";

export default class TransactionTypeServices {
    static async createTransactionType(transactionType) {
        const transactionTypeRecord = await TransactionType.create(transactionType);
        return {transactionType: transactionTypeRecord};
    }

    static async listTransactionTypes() {
        const transactionTypeRecords = await TransactionType.find({})
        return {transactionTypes: transactionTypeRecords};
    };

    static async getTransactionType(transactionTypeId) {
        const transactionTypeRecord = await TransactionType.findById(transactionTypeId);
        return {transactionType: transactionTypeRecord};
    };
}