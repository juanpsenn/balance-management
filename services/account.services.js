import Account from "../models/account";

export default class AccountServices {
    static async createAccount(account) {
        const accountRecord = await Account.create(account);
        return {account: accountRecord};
    }

    static async listAccounts() {
        const accountRecords = await Account.find({}).populate(
            [
                {path: 'bank', select: 'name'},
                {path: 'owner', select: 'name'}
            ])
        return {accounts: accountRecords};
    };

    static async getAccount(accountId, session) {
        const accountRecord = await Account.findById(accountId).session(session).populate(
            [
                {path: 'bank', select: 'name'},
                {path: 'owner', select: 'name'}
            ]);
        return {account: accountRecord};
    };

    static async updateBalance(transaction, session = null) {
        const {account} = await this.getAccount(transaction.account, session)

        account.balance += transaction.amount;
        account.save();

        return {account: account};
    };
}