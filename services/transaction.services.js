import Transaction from "../models/transaction";
import AccountServices from "./account.services";
import mongoose from "mongoose";

export default class TransactionService {

    static async registerTransaction(transaction) {
        const session = await mongoose.startSession();
        session.startTransaction();
        try {
            const transactionRecord = await Transaction.create([transaction], {session, new: true});
            await AccountServices.updateBalance(transaction, session);
            await session.commitTransaction();
            return {transaction: transactionRecord};
        } catch (e) {
            await session.abortTransaction();
            throw new Error(e.message)
        } finally {
            session.endSession();
        }

    };

    static async listTransactions() {
        const transactionRecords = await Transaction.find({})
            .populate('type', 'name');
        return {transactions: transactionRecords};
    };

    static async getTransaction(transactionId) {
        const transactionRecord = await Transaction.findById(transactionId);
        return {transaction: transactionRecord};
    };
};