import {Schema, model} from 'mongoose';

const Owner = new Schema({
        name: {
            type: String,
            required: true,
        }
    },
    {timestamps: true}
);

export default model('Owner', Owner);