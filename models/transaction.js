import {Schema, model} from "mongoose";

const Transaction = new Schema(
    {
        datetime: {
            type: Date,
            required: true
        },
        amount: {
            type: Number,
            required: true
        },
        type: {
            type: Schema.Types.ObjectId,
            ref: 'TransactionType'
        },
        observation: {
            type: String
        },
        account: {
            type: Schema.Types.ObjectId,
            ref: 'Account',
        },
    },
    {timestamps: true}
);

export default model('Transaction', Transaction);