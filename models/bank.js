import {Schema, model} from 'mongoose';

const Bank = new Schema({
        name: {
            type: String,
            required: true,
            unique: true
        }
    },
    {timestamps: true}
);

export default model('Bank', Bank);