import {Schema, model} from "mongoose";

const TransactionType = new Schema(
    {
        name: {
            type: String,
            required: true
        },
        description: {
            type: String,
        },
        droppedAt: {
            type: Date
        }
    },
    {timestamps: true}
);

export default model('TransactionType', TransactionType);