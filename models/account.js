import {Schema, model} from "mongoose";

const Account = new Schema({
        balance: {
            type: Number,
            default: 0.00,
            required: true
        },
        droppedAd: {
            type: Date
        },
        bank: {
            type: Schema.Types.ObjectId,
            ref: 'Bank'
        },
        owner: {
            type: Schema.Types.ObjectId,
            ref: 'Owner',
            required: true
        }
    },
    {timestamps: true}
);

export default model('Account', Account);