import 'dotenv/config';
import express from 'express';
import mainLoader from './loaders';

async function startServer() {
    const app = express();
    await mainLoader(app);

    app.listen(process.env.PORT, () => {
        console.log(`Running on port ${process.env.PORT}!`)
    });
}

startServer();