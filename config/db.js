const db = process.env.DATABASE.replace(
    '<password>',
    process.env.DATABASE_PASSWORD
);

export default db;